package com.project.di

import co.core.imageloader.NImageLoader
import com.base.imageloader.FrescoImageLoaderImpl
import com.project.MyApplication
import com.project.api.Api
import com.project.api.OkHttpApiImpl
import com.project.api.StethoInterceptor
import com.project.api.logging.HttpLoggingInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

/**
 * Created by freesky1102 on 8/14/16.
 */
@Module
class ApplicationModule(private val application: MyApplication) {

    @Provides
    fun provideImageLoader(): NImageLoader {
        return FrescoImageLoaderImpl(application)
    }

    @Provides
    fun provideApi(): Api {

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val okHttpClient = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addNetworkInterceptor(StethoInterceptor())
                .build()

        return OkHttpApiImpl(okHttpClient)
    }
}
