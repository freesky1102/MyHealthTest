package com.base.adapters

import android.support.v7.widget.RecyclerView
import android.view.View

class AppViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)