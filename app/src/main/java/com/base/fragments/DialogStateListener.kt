package com.base.fragments

interface DialogStateListener {

    fun onShowDialog()

    fun onDismissDialog()
}
