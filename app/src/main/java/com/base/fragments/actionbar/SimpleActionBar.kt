package com.base.fragments.actionbar

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.core.actionbar.CustomActionbar

/**
 * Created by freesky1102 on 6/25/17.
 */
class SimpleActionBar(containerView: ViewGroup, inflater: LayoutInflater, @LayoutRes contentLayoutRes: Int) : CustomActionbar {

    private val mContainerView: ViewGroup = containerView
    private val mLayoutInflater: LayoutInflater = inflater
    private lateinit var mActionbarView: ViewGroup

    init {
        val contentView: View = inflater.inflate(contentLayoutRes, mContainerView, false)
        mContainerView.addView(contentView)
    }

    override fun hide() {
        mActionbarView.visibility = View.GONE
    }

    override fun show() {
        mActionbarView.visibility = View.VISIBLE
    }

    override fun inflateLayout(layoutRes: Int) {
        mActionbarView = mLayoutInflater.inflate(layoutRes, mContainerView, false) as ViewGroup
        mContainerView.addView(mActionbarView, 0)
    }

    override val actionbarView: ViewGroup
        get() = mActionbarView
}
