package com.base.fragments.actionbar

/**
 * Created by TungDX on 5/27/2015.
 */
interface ActionbarInfo {
    val actionbarTitle: String
}
