package com.base.fragments.toolbox

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentManager.OnBackStackChangedListener
import co.core.activities.NActivity
import co.core.fragments.NavigationManager
import com.base.R
import java.util.*

class AppNavigationManager(private var mActivity: NActivity?, frameId: Int) : NavigationManager {
    private val mBackStack: Stack<NavigationState> = MainThreadStack()
    private var mFragmentManager: FragmentManager? = null
    var currentPlaceholder: Int = 0
        private set

    init {
        currentPlaceholder = frameId
        mFragmentManager = mActivity!!.supportFragmentManager
    }

    fun addOnBackStackChangedListener(
            backStackChangedListener: OnBackStackChangedListener) {
        mFragmentManager!!
                .addOnBackStackChangedListener(backStackChangedListener)
    }

    fun removeOnBackStackChangedListener(
            onbackstackchangedlistener: OnBackStackChangedListener) {
        mFragmentManager!!
                .removeOnBackStackChangedListener(onbackstackchangedlistener)
    }

    /**
     * Check whether can navigate or not.

     * @return true if can navigate, false otherwise.
     */
    protected fun canNavigate(): Boolean {
        return mActivity != null && mActivity!!.canChangeFragmentManagerState()
    }

    override fun goBack(): Boolean {
        if (mActivity == null || !mActivity!!.canChangeFragmentManagerState()
                || mBackStack.isEmpty() || mFragmentManager!!.backStackEntryCount == 0)
            return false
        mBackStack.pop()
        mFragmentManager!!.popBackStack()
        // Even popped back stack, the fragment which is added without addToBackStack would be showing.
        // So we need to remove it manually
        val transaction = mFragmentManager!!.beginTransaction()
        val currentFrag = mFragmentManager!!.findFragmentById(currentPlaceholder)
        if (currentFrag != null) {
            transaction.remove(currentFrag)
            transaction.commit()
        }

        return true
    }

    override fun finishActivity() {
        if (mActivity == null) {
            return
        }
        mActivity!!.finish()
    }

    override fun showPage(fragment: Fragment) {
        showPage(fragment, true, true)
    }

    override fun showPage(fragment: Fragment, hasAnimation: Boolean, isAddBackStack: Boolean) {
        if (!canNavigate())
            return
        val transaction = mFragmentManager!!.beginTransaction()
        if (hasAnimation)
            transaction.setCustomAnimations(R.anim.fragment_enter,
                    R.anim.fragment_exit, R.anim.fragment_pop_enter,
                    R.anim.fragment_pop_exit)
        transaction.replace(currentPlaceholder, fragment)
        val navigationState = NavigationState(currentPlaceholder)
        if (isAddBackStack) {
            transaction.addToBackStack(navigationState.backStackName)
            mBackStack.push(navigationState)
        }
        transaction.commit()

    }

    /**
     * Terminate resource that keep by this class. Must call in
     * [Activity.onDestroy]
     */
    fun terminate() {
        mFragmentManager = null
        mActivity = null
    }

    val currentPlaceholderInBackstack: Int
        get() = if (!mBackStack.isEmpty())
            (mBackStack.peek() as NavigationState).placeholder
        else
            0

    val isBackStackEmpty: Boolean
        get() = mFragmentManager!!.backStackEntryCount <= 0

    fun isPlaceHolderEmpty(placeHolder: Int): Boolean {
        if (!mBackStack.isEmpty()) {
            mBackStack
                    .filter { it is NavigationState && it.placeholder == placeHolder }
                    .forEach { return true }
        }
        return false
    }

    val backStackSize: Int
        get() = mBackStack.size

    override val activePage: Fragment?
        get() = mFragmentManager!!.findFragmentById(currentPlaceholder)

    /**
     * Use for save state of Navigation Manager, called in
     * [Activity.onSaveInstanceState]

     * @param bundle
     */
    fun serialize(bundle: Bundle) {
        if (!mBackStack.isEmpty())
            bundle.putParcelableArrayList("nm_state", java.util.ArrayList(mBackStack))
    }

    /**
     * Use for restore state that saved in [.serialize], called in
     * [Activity.onCreate]

     * @param bundle
     */
    fun deserialize(bundle: Bundle?) {
        if (bundle == null) return
        val arrayList = bundle.getParcelableArrayList<NavigationState>("nm_state")
        if (arrayList != null && arrayList.size != 0) {
            for (navigationState in arrayList) {
                mBackStack.push(navigationState)
            }
        }
    }
}
