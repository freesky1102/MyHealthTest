package com.base.fragments

/**
 * @author TUNGDX
 */

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import co.core.actionbar.CustomActionbar
import co.core.fragments.NFragment
import com.base.R
import com.base.fragments.actionbar.SimpleActionBar
import com.project.api.Api
import com.project.dialog.LoadingDialogFragment
import com.project.screens.BaseView

/**
 * This is base fragment. <br></br>
 * It contains some default attributes: Context, Api, ImageLoader,
 * NavigationManager, Actionbar <br></br>
 */
abstract class BaseAppFragment : NFragment(), BaseView {

    protected var mApi: Api? = null

    protected var mActionbar: CustomActionbar? = null

    private val mHandler: Handler by lazy { Handler(Looper.getMainLooper()) }

    init {
        arguments = Bundle()
    }

    @CallSuper
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mPageFragmentHost = mPageFragmentHost

        if (mPageFragmentHost is AppFragmentHost) {
            mApi = mPageFragmentHost.dfeApi
        }
    }

    override val isHasActionbar: Boolean
        get() = true

    override val layoutRes: Int
        @LayoutRes
        get() = 0

    /**
     * Shouldn't override this function...Use [.getLayoutRes]
     */
    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        if (!isHasActionbar) {
            val layoutRes = layoutRes
            if (layoutRes != 0) {
                return inflater.inflate(layoutRes, container, false)
            } else
                return null
        } else {

            val frame = inflater.inflate(R.layout.fragment_frame_has_actionbar, container, false) as LinearLayout
            mActionbar = SimpleActionBar(frame, inflater, layoutRes)

            return frame
        }
    }

    override fun showLoadingBar() {
        val loading = childFragmentManager.findFragmentByTag(LOADING_TAG)
        if (loading != null) return

        LoadingDialogFragment().show(childFragmentManager, LOADING_TAG)
    }

    override fun hideLoadingBar() {
        mHandler.postDelayed(Runnable {
            val loading = childFragmentManager.findFragmentByTag(LOADING_TAG) ?: return@Runnable

            (loading as DialogFragment).dismissAllowingStateLoss()
        }, 500)
    }

    protected fun showDialog(dialog: DialogFragment?) {
        dialog?.show(childFragmentManager, DIALOG_TAG)
    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()

        mHandler.removeCallbacksAndMessages(null)
    }

    override val isReadyToChangeView: Boolean
        get() {
            activity ?: return false
            if (!isAdded) return false
            if (isDetached) return false
            if (isRemoving) return false
            return !isHidden
        }

    companion object {
        const val DIALOG_TAG = "dialog_tag"
        const val LOADING_TAG = "loading_tag"
    }
}
