package com.base.fragments

import android.app.Activity
import android.widget.Toast

import com.project.api.errors.BaseError

import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by freesky1102 on 9/12/16.
 */
object ActionUtil {

    fun showToast(activity: Activity?, message: String?) {
        if (activity == null || message == null) return
        activity.runOnUiThread { Toast.makeText(activity, message, Toast.LENGTH_SHORT).show() }
    }

    @JvmOverloads
    fun doIfError(fragment: BaseAppFragment?, e: Exception?, dialogRequestCode: Int = 0) {
        if (fragment == null) return

        fragment.hideLoadingBar()

        if (e == null) return

        val activity = fragment.activity
        if (activity == null || activity.isFinishing) return

        if (e is BaseError) {

            var message: String?
            try {
                message = e.response.body()?.string()
            } catch (e1: IOException) {
                e1.printStackTrace()
                message = e1.message
            }

            //TODO error from server

        } else if (e is UnknownHostException || e is ConnectException || e is SocketTimeoutException) {
            //TODO no internet connection
        } else {
            val msg = e.message

            //TODO unknown error

        }
    }
}
