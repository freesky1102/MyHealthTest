package com.base.fragments

import co.core.NFragmentHost
import com.project.api.Api

/**
 * @author TUNGDX
 */
interface AppFragmentHost : NFragmentHost {

    val dfeApi: Api?

}
