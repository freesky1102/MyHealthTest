//   Copyright 2012 Matteo Catena (catena.matteo@gmail.com)
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package com.tsp.eu;

import java.util.Collection;

/**
 * This interface exposes a method calculateRoute to obtain the shortest trip between a
 * collection of locations.
 * It solves a TSP problem.
 *
 * @author Matteo Catena (catena.matteo@gmail.com)
 */
public interface TspService {
    /**
     * This method generate the shortest trip between a set of locations.
     *
     * @param locations The first element should be the origin and the end of the trip
     * @return a route represented as an ordered collection of strings
     */
    public Collection<String> calculateRoute(Collection<String> locations);
}
