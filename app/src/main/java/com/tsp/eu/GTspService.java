//   Copyright 2012 Matteo Catena (catena.matteo@gmail.com)
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package com.tsp.eu;

import com.google.gson.Gson;
import com.tsp.de.Path;
import com.tsp.de.TspSolver;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;


/**
 * @author Matteo Catena
 */
public class GTspService implements TspService {

    //Please notice that GTspService uses the Google's Distance Matrix API.
    //Use of the Distance Matrix API must relate to the display of information on a Google Map;
    //for example, to determine origin-destination pairs that fall within a specific driving time
    // from one another,
    //before requesting and displaying those destinations on a map.
    //Use of the service in an application that doesn't display a Google map is prohibited.
    private static final String SERVICE_URL =
            "http://maps.googleapis.com/maps/api/distancematrix/json";

    private String implodeLocations(Collection<String> locations) {
        String[] locationArray = new String[locations.size()];
        locationArray = locations.toArray(locationArray);

        String implodedString = null;
        if (locationArray.length == 0) {
            implodedString = "";
        } else {
            StringBuffer sb = new StringBuffer();
            sb.append(locationArray[0]);
            for (int i = 1; i < locationArray.length; i++) {
                sb.append("|");
                sb.append(locationArray[i]);
            }
            implodedString = sb.toString();
        }

        return implodedString;
    }

    public Collection<String> calculateRoute(Collection<String> locations) {
        if (locations == null || locations.size() == 0) {
            return null;
        }
        if (locations.size() >= 1 && locations.size() <= 2) {
            return locations;
        }

        GDistanceMatrixResponse response = null;
        response = getResponse(locations);

        double[][] distances = getDistances(response);
        TspSolver solver = new TspSolver(distances);
        Path p = solver.solve(20);

        return buildResult(p, locations);
    }

    private Collection<String> buildResult(Path p, Collection<String> locations) {
        String[] locationArray = new String[locations.size()];
        locationArray = locations.toArray(locationArray);

        Collection<String> result = new ArrayList<String>();

        int i = 0;
        do {
            result.add(locationArray[i]);
            i = p.To[i];
        } while (i != 0);

        return result;
    }

    private double[][] getDistances(GDistanceMatrixResponse response) {
        double[][] distances = new double[response.getRows().length][response
                .getRows().length];
        for (int i = 0; i < response.getRows().length; i++) {
            for (int j = 0; j < response.getRows()[i].getElements().length; j++) {
                distances[i][j] = response.getRows()[i].getElements()[j]
                        .getDistance().getValue();
            }
        }
        return distances;
    }

    private GDistanceMatrixResponse getResponse(Collection<String> locations) {
        InputStreamReader inputStreamReader = null;
        InputStream inputStream = null;
        try {
            if (locations == null || locations.isEmpty()) {
                return null;
            }
            Collection<String> encodedLocations = new ArrayList<String>();
            for (String s : locations) {
                String encoded = URLEncoder.encode(s, "UTF-8");
                encodedLocations.add(encoded);
            }
            String implodedLocations = implodeLocations(encodedLocations);
            if (implodedLocations == null || implodedLocations.isEmpty()) {
                return null;
            }

            StringBuilder stringBuilder = new StringBuilder(SERVICE_URL);
            stringBuilder.append("?origins=");
            stringBuilder.append(implodedLocations);
            stringBuilder.append("&destinations=");
            stringBuilder.append(implodedLocations);
            stringBuilder.append("&sensor=false");

            Gson gson = new Gson();
            inputStream = new URL(stringBuilder.toString()).openStream();
            inputStreamReader = new InputStreamReader(inputStream);
            GDistanceMatrixResponse matrix = gson.fromJson(
                    inputStreamReader, GDistanceMatrixResponse.class);
            inputStreamReader.close();
            return matrix;
        } catch (IOException e) {
            e.printStackTrace();
            try {
                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException io) {
                io.printStackTrace();
            }
            return null;
        }
    }

}
