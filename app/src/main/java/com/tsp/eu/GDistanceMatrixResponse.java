//   Copyright 2012 Matteo Catena (catena.matteo@gmail.com)
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
package com.tsp.eu;

/**
 * @author Matteo Catena
 */
public class GDistanceMatrixResponse {

    private String[] destination_addresses;

    private String[] origin_addresses;

    private GDistanceMatrixRow[] rows;

    private String status;

    public String[] getDestinationAddresses() {
        return destination_addresses;
    }

    public void setDestinationAddresses(String[] destination_addresses) {
        this.destination_addresses = destination_addresses;
    }

    public String[] getOriginAddresses() {
        return origin_addresses;
    }

    public void setOriginAddresses(String[] origin_addresses) {
        this.origin_addresses = origin_addresses;
    }

    public GDistanceMatrixRow[] getRows() {
        return rows;
    }

    public void setRows(GDistanceMatrixRow[] rows) {
        this.rows = rows;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
