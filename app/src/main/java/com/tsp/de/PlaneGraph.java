/**
 * Original code by Rene Grothmann, modified by Matteo Catena
 *
 * @author Rene Grothmann (http://www.rene-grothmann.de/)
 * @author Matteo Catena (catena.matteo@gmail.com)
 */
package com.tsp.de;

public class PlaneGraph extends Graph {
    final double sqr(double x) {
        return x * x;
    }

    public PlaneGraph(double[][] distances) {
        super(distances.length);
        int i, j;
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++) {
                connect(i, j, distances[i][j]);
            }
        }
    }
}
