/**
 * Original code by Rene Grothmann, modified by Matteo Catena
 *
 * @author Rene Grothmann (http://www.rene-grothmann.de/)
 * @author Matteo Catena (catena.matteo@gmail.com)
 */
package com.tsp.de;

import java.util.Random;


public class TspSolver {

    private Graph graph;

    public TspSolver(double[][] distances) {
        graph = new PlaneGraph(distances);
    }

    public Path solve(int iterations) {
        Random R = new Random();

        Path pa = new Path(graph);
        Path pmin = null;

        double lmin = 1e50;
        int count = 0;

        do {
            pa.random(R);
            pa.localoptimize();
            if (pa.length() < lmin - 1e-10) {
                pmin = (Path) pa.clone();
                lmin = pa.length();
                pa.getlength();
                count = 0;
            } else {
                count++;
            }
        } while (count < iterations);
        return pmin;
    }
}
