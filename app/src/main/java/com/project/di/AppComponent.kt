package com.project.di

import com.project.MyApplication

import dagger.Component
import javax.inject.Singleton

/**
 * Created by freesky1102 on 8/14/16.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface AppComponent {

    fun inject(myApplication: MyApplication)

}
