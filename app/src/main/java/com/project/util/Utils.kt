package com.project.util

import java.util.ArrayList

class Utils {

    interface NewObjectClosure<DEST> {

        fun makeNew(): DEST

    }

    interface InitObjectClosure<DEST, SOURCE> {

        fun initDest(dest: DEST, source: SOURCE)

    }

    interface ExcludeClosure<SOURCE> {

        fun isExclude(source: SOURCE): Boolean

    }

    class TransformerV2<DEST, SOURCE>(internal val newObjectClosure: NewObjectClosure<DEST>,
                                      internal val initObjectClosure: InitObjectClosure<DEST, SOURCE>?,
                                      internal val excludeClosure: ExcludeClosure<SOURCE>?)

    companion object {

        fun <DEST, SOURCE> transform(sources: List<SOURCE>?,
                                     transformer: TransformerV2<DEST, SOURCE>): ArrayList<DEST>? {

            if (sources == null) return null

            if (sources.isEmpty()) return null

            val dests = ArrayList<DEST>()

            for (index in sources.indices) {
                val sourceObject = sources[index] ?: continue

                if (transformer.excludeClosure != null && transformer.excludeClosure.isExclude(sourceObject)) {
                    continue
                }

                val dest = transformer.newObjectClosure.makeNew()

                if (transformer.initObjectClosure != null) {
                    transformer.initObjectClosure.initDest(dest, sourceObject)
                }

                dests.add(dest)
            }

            return dests
        }
    }


}