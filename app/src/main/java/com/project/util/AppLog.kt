package com.project.util

import android.util.Log

import com.base.BuildConfig

/**
 * Created by freesky1102 on 7/10/15.
 */
object AppLog {

    private val isEnabled = BuildConfig.DEBUG

    private val TAG = "My App"

    fun d(tag: String?, message: String?) {
        if (!isEnabled) return
        if (message == null || tag == null) return
        Log.d(tag, message)
    }

    fun d(message: String?) {
        if (!isEnabled) return
        if (message == null) return
        Log.d(TAG, message)
    }

    fun e(message: String?) {
        if (!isEnabled) return
        if (message == null) return
        Log.e(TAG, message)
    }

    fun i(message: String?) {
        if (!isEnabled) return
        if (message == null) return
        Log.i(TAG, message)
    }

    fun i(tag: String?, message: String?) {
        if (!isEnabled) return
        if (message == null || tag == null) return
        Log.i(tag, message)
    }

    fun e(tag: String?, message: String?) {
        if (!isEnabled) return
        if (message == null || tag == null) return
        Log.e(tag, message)
    }

    fun w(tag: String?, message: String?) {
        if (!isEnabled) return
        if (message == null || tag == null) return
        Log.w(tag, message)
    }

    fun w(message: String?) {
        if (!isEnabled) return
        if (message == null) return
        Log.w(TAG, message)
    }
}
