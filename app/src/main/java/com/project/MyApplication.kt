package com.project

import co.core.imageloader.NImageLoader
import com.project.api.Api
import com.project.di.ApplicationModule
import com.project.di.DaggerAppComponent
import com.project.util.AppLog
import javax.inject.Inject

/**
 * Created by freesky1102 on 5/14/16.
 */
class MyApplication : BuildVariantApplication() {

    @Inject
    lateinit var imageLoader: NImageLoader

    @Inject
    lateinit var api: Api

    override fun onCreate() {
        super.onCreate()
        myApplicationInstance = this

        val appComponent = DaggerAppComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
        appComponent.inject(this)

        AppRepos.setUp(api)

        AppLog.d("ASdASD")
    }

    companion object {

        private lateinit var myApplicationInstance: MyApplication

        fun get(): MyApplication {
            return myApplicationInstance
        }
    }
}
