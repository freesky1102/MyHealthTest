package com.project

object AppConfig {

    const val MY_JSON_URL = "https://api.myjson.com/bins/clscy?pretty=1"

    const val GOOGLE_MATRIX_URL = "https://maps.googleapis.com/maps/api/distancematrix"

    const val GOOGLE_DIRECTION_URL = "https://maps.googleapis.com/maps/api/directions"
}