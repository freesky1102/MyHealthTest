package com.project.activities

import android.os.Bundle
import android.view.KeyEvent
import co.core.activities.NActivity
import co.core.fragments.NavigationManager
import co.core.imageloader.NImageLoader
import com.base.R
import com.base.fragments.AppFragmentHost
import com.project.api.Api
import com.project.screens.map.MapFragment
import com.project.toolbox.SimpleActivityController

class MainActivity : NActivity(), AppFragmentHost {

    private var mDelegate: SimpleActivityController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDelegate = SimpleActivityController(this)

        val navigationManager = mDelegate!!.navigationManager
        val currentFragment = navigationManager!!.activePage
        if (currentFragment == null) {
            navigationManager.showPage(MapFragment(), false, false)
        }
    }

    override val dfeApi: Api?
        get() = mDelegate?.dfeApi

    override val imageLoader: NImageLoader?
        get() = mDelegate?.imageLoader

    override val navigationManager: NavigationManager?
        get() = mDelegate?.navigationManager

    override fun onDestroy() {
        super.onDestroy()
        mDelegate?.doOnDestroy()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        val isHandled = mDelegate!!.doDuringOnKeyDown(keyCode, event)

        if (!isHandled)
            return super.onKeyDown(keyCode, event)

        return true
    }

    override fun onBackPressed() {
        if (!mDelegate!!.doOnBackPress())
            super.onBackPressed()
    }
}
