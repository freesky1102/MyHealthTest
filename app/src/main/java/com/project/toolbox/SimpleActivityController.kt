package com.project.toolbox

import android.view.KeyEvent
import co.core.activities.NActivity
import co.core.fragments.NavigationManager
import co.core.imageloader.NImageLoader
import com.base.R
import com.base.fragments.AppFragmentHost
import com.base.fragments.actionbar.ActionbarBackHandler
import com.base.fragments.toolbox.AppNavigationManager
import com.project.MyApplication
import com.project.api.Api

/**
 * Created by freesky1102 on 8/14/16.
 */
class SimpleActivityController(activity: NActivity) : AppFragmentHost {

    private var mImageLoader: NImageLoader? = null
    private var mApi: Api? = null
    private var mNavigationManager: AppNavigationManager? = null

    override var imageLoader: NImageLoader? = null
        get() = mImageLoader

    override var dfeApi: Api? = null
        get() = mApi

    override val navigationManager: NavigationManager?
        get() = mNavigationManager

    init {
        mImageLoader = MyApplication.get().imageLoader
        mApi = MyApplication.get().api
        mNavigationManager = AppNavigationManager(activity, R.id.content_layout)
    }

    fun doOnDestroy() {
        mImageLoader = null
        mApi = null
        mNavigationManager?.terminate()
    }

    /**
     * @return true if handled, else return false
     */
    fun doDuringOnKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return callBackHandlerOnActivePage()
        }

        return false
    }

    fun doOnBackPress(): Boolean {
        return callBackHandlerOnActivePage()
    }

    private fun callBackHandlerOnActivePage(): Boolean {
        val activePage = mNavigationManager!!.activePage

        var isHandled = false

        if (activePage is ActionbarBackHandler) {
            isHandled = activePage.onBackHandled()
        }

        if (!isHandled)
            isHandled = mNavigationManager!!.goBack()

        return isHandled
    }
}
