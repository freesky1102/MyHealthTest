package com.project.api.errors

import okhttp3.Call
import okhttp3.Response

/**
 * Created by freesky1102 on 7/18/16.
 */
class AuthFailureError(call: Call, response: Response) : BaseError(call, response)
