package com.project.api.errors

import okhttp3.Call
import okhttp3.Response

/**
 * Created by freesky1102 on 7/18/16.
 */
abstract class BaseError(val call: Call, val response: Response) : Exception()
