package com.project.api

import android.support.annotation.MainThread


interface FailedClosure {

    @MainThread
    fun onFailed(message: String)

}
