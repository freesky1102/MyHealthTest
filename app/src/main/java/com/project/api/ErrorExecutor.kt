package com.project.api

import android.content.Context
import android.support.annotation.MainThread
import com.base.R
import com.project.MyApplication
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object ErrorExecutor {

    @MainThread
    fun <DATA, T : BaseCallback<DATA>> execute(callback: T?, e: Exception) {

        if (callback == null) return

        val context = MyApplication.get()

        val errorInteract: ErrorInteract<Exception>

        if (e is UnknownHostException || e is ConnectException) {
            errorInteract = NoNetworkErrorHandler(context, callback)

        } else if (e is SocketTimeoutException) {
            errorInteract = TimeOutErrorHandler(context, callback)

        } else {
            errorInteract = FailErrorHandler(context, callback)
        }

        errorInteract.execute(e)
    }

    internal interface ErrorInteract<in E : Exception> {

        fun execute(e: E)

    }

    internal class NoNetworkErrorHandler(var context: Context, var callBack: BaseCallback<*>) : ErrorInteract<Exception> {

        override fun execute(e: Exception) {
            val message = context.getString(R.string.no_internet_connection)
            callBack.onNetworkError(message)
        }
    }

    internal class FailErrorHandler(var context: Context, var callBack: BaseCallback<*>) : ErrorInteract<Exception> {

        override fun execute(e: Exception) {
            val message = context.getString(R.string.something_went_wrong)
            callBack.onFailed(message)
        }
    }

    internal class TimeOutErrorHandler(var context: Context, var callback: BaseCallback<*>) : ErrorInteract<Exception> {

        override fun execute(e: Exception) {
            val message = context.getString(R.string.timeout_message)
            callback.onTimeout(message)
        }
    }
}
