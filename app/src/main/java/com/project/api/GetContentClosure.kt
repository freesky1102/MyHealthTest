package com.project.api

import okhttp3.Call
import okhttp3.Response


interface GetContentClosure {

    @Throws(Exception::class)
    fun getContent(call: Call, response: Response): String

}
