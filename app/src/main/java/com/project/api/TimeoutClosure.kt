package com.project.api

import android.support.annotation.MainThread

interface TimeoutClosure {

    @MainThread
    fun onTimeout(message: String)

}