package com.project.api

interface BaseCallback<T> : SuccessClosure<T>, NetworkErrorClosure, FailedClosure, TimeoutClosure
