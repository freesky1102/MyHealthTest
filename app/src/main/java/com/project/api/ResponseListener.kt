package com.project.api


import android.support.annotation.WorkerThread
import com.project.util.ThreadUtils.onMain
import okhttp3.Call
import okhttp3.Response

/**
 * Created by freesky1102 on 3/23/18.
 */

class ResponseListener<C : BaseCallback<VM>, RESPONSE, VM> {

    private var contentClosure: GetContentClosure? = null

    private var onErrorClosure: OnErrorClosure? = null

    private var parseClosure: ParseClosure<RESPONSE>? = null

    private var onResponseClosure: OnResponseClosure<RESPONSE>? = null

    internal var transform: TransformDataClosure<RESPONSE, VM>? = null

    internal var callback: C? = null

    constructor(callback: C, parseClosure: ParseClosure<RESPONSE>,
                transform: TransformDataClosure<RESPONSE, VM>) {
        this.callback = callback
        this.parseClosure = parseClosure
        this.transform = transform

        this.contentClosure = DefaultContentClosure()
        this.onErrorClosure = DefaultErrorClosure()
        this.onResponseClosure = DefaultResponseClosure()
    }

    constructor(callback: C, parseClosure: ParseClosure<RESPONSE>,
                onResponseClosure: OnResponseClosure<RESPONSE>) {

        this.callback = callback
        this.parseClosure = parseClosure
        this.onResponseClosure = onResponseClosure

        this.contentClosure = DefaultContentClosure()
        this.onErrorClosure = DefaultErrorClosure()
    }

    @Throws(Exception::class)
    internal fun parse(call: Call, content: String, okResponse: Response): RESPONSE? {

        var response: RESPONSE? = null

        if (parseClosure != null) {
            response = parseClosure!!.parse(call, content, okResponse)
        }

        return response
    }

    @Throws(Exception::class)
    internal fun getContent(call: Call, response: Response): String? {

        return if (contentClosure != null) {
            contentClosure!!.getContent(call, response)
        } else null

    }

    @WorkerThread
    @Throws(Exception::class)
    internal fun onResponse(response: RESPONSE) {
        if (onResponseClosure != null) {
            onResponseClosure!!.onResponse(response)
        }
    }

    @WorkerThread
    internal fun onError(e: Exception) {
        if (onErrorClosure != null) {
            onErrorClosure!!.onError(e)
        }
    }

    private class DefaultContentClosure : GetContentClosure {

        @Throws(Exception::class)
        override fun getContent(call: Call, response: Response): String {
            return response.body()!!.string()
        }
    }

    private inner class DefaultErrorClosure : OnErrorClosure {

        override fun onError(e: Exception) {
            onMain(Runnable {
                ErrorExecutor.execute(callback, e)
            })
        }
    }

    private inner class DefaultResponseClosure : OnResponseClosure<RESPONSE> {

        @Throws(Exception::class)
        fun parseVM(response: RESPONSE): VM {
            var viewModel: VM? = null

            if (transform != null) {
                viewModel = transform!!.transformFrom(response)
            }

            return viewModel!!
        }

        @Throws(Exception::class)
        override fun onResponse(response: RESPONSE) {
            if (callback == null) return

            val finalModel = parseVM(response)
            onMain(Runnable {
                callback?.onSuccess(finalModel)
            })
        }
    }
}
