package com.project.api

import android.support.annotation.MainThread

interface SuccessClosure<in T> {

    @MainThread
    fun onSuccess(data: T)

}