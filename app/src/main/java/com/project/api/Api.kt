package com.project.api

/**
 * Created by freesky1102 on 5/14/16.
 */
interface Api {

    fun getLocations(request: AppRequest)

    fun getMatrix(request: AppRequest)

    fun getGGDirection(request: AppRequest)

}
