package com.project.api

import android.support.annotation.WorkerThread

interface OnResponseClosure<in R> {

    @WorkerThread
    @Throws(Exception::class)
    fun onResponse(response: R)

}
