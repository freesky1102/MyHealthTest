package com.project.api.source

import com.project.api.*
import com.project.screens.BaseView
import com.project.screens.NetworkInteractView

class AppCallback<T> : BaseCallback<T> {

    private var view: BaseView? = null

    private var successClosure: SuccessClosure<T>? = null

    private var failedClosure: FailedClosure? = null

    private var networkErrorClosure: NetworkErrorClosure? = null

    private var timeoutClosure: TimeoutClosure? = null

    private val isShouldNotifyEvent: Boolean
        get() = view == null || view!!.isReadyToChangeView

    constructor  (view: BaseView,
                  successClosure: SuccessClosure<T>) {

        initRequestErrorDefault()

        this.view = view
        this.successClosure = successClosure
    }

    private constructor() {}

    private fun initRequestErrorDefault() {
        failedClosure = DefaultFailedClosure()
        networkErrorClosure = DefaultNetworkErrorClosure()
        timeoutClosure = DefaultTimeoutClosure()
    }

    override fun onFailed(message: String) {
        if (!isShouldNotifyEvent) return

        if (failedClosure != null) {
            failedClosure!!.onFailed(message)
        }
    }

    override fun onNetworkError(message: String) {
        if (!isShouldNotifyEvent) return

        if (networkErrorClosure != null) {
            networkErrorClosure!!.onNetworkError(message)
        }
    }

    override fun onSuccess(data: T) {
        if (!isShouldNotifyEvent) return

        if (successClosure != null) {
            successClosure!!.onSuccess(data)
        }
    }

    override fun onTimeout(message: String) {
        if (!isShouldNotifyEvent) return

        if (timeoutClosure != null) {
            timeoutClosure!!.onTimeout(message)
        }
    }

    private inner class DefaultFailedClosure : FailedClosure {

        override fun onFailed(message: String) {
            view!!.hideLoadingBar()
            (view as NetworkInteractView).displayUnexpectedError(message)
        }
    }

    private inner class DefaultNetworkErrorClosure : NetworkErrorClosure {

        override fun onNetworkError(message: String) {
            view!!.hideLoadingBar()
            (view as NetworkInteractView).displayNetworkError(message)
        }
    }

    private inner class DefaultTimeoutClosure : TimeoutClosure {

        override fun onTimeout(message: String) {
            view!!.hideLoadingBar()
            (view as NetworkInteractView).displayTimeoutError(message)
        }
    }
}