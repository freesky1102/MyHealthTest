package com.project.api.source

import com.project.model.DirectionVM
import com.project.model.LocationVM
import com.project.model.param.MatrixParam
import com.tsp.eu.GDistanceMatrixResponse

interface DirectionDataSource {

    fun callMatrix(matrixParam: MatrixParam, callback: AppCallback<GDistanceMatrixResponse>)

    fun callLocations(callback: AppCallback<ArrayList<LocationVM>>)

    fun callDirection(origin: LocationVM, wayPoints: Array<LocationVM>?, dest: LocationVM, callback: AppCallback<DirectionVM>)

}