package com.project.api.source

import com.base.R
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.maps.android.PolyUtil
import com.project.MyApplication
import com.project.api.*
import com.project.model.DirectionVM
import com.project.model.LocationVM
import com.project.model.param.MatrixParam
import com.project.model.response.GGDirectionResponse
import com.project.model.response.MyJsonResponse
import com.project.util.ThreadUtils.onMain
import com.project.util.Utils
import com.tsp.eu.GDistanceMatrixResponse
import okhttp3.Call
import okhttp3.Response
import java.math.BigInteger

class DirectionCloudDataSource(private val api: Api) : DirectionDataSource {

    override fun callLocations(callback: AppCallback<ArrayList<LocationVM>>) {
        val request = AppRequest.Builder().get()
                .setResponseListener(ResponseListener(callback, object : ParseClosure<MyJsonResponse> {
                    override fun parse(call: Call, content: String, okResponse: Response): MyJsonResponse {
                        return Gson().fromJson(content, MyJsonResponse::class.java)
                    }
                }, object : TransformDataClosure<MyJsonResponse, ArrayList<LocationVM>> {
                    override fun transformFrom(response: MyJsonResponse): ArrayList<LocationVM> {
                        return response.location!!
                    }
                }))
                .build()

        api.getLocations(request)
    }

    override fun callMatrix(matrixParam: MatrixParam, callback: AppCallback<GDistanceMatrixResponse>) {

        val params = HashMap<String, String>()

        params["mode"] = "driving"
        params["units"] = "metric"
        params["origins"] = convert(matrixParam.origins)
        params["destinations"] = convert(matrixParam.destination)

        val request = AppRequest.Builder()
                .get()
                .setQueryParams(params)
                .setPathSegments("json")
                .setResponseListener(ResponseListener(callback, object : ParseClosure<GDistanceMatrixResponse> {
                    override fun parse(call: Call, content: String, okResponse: Response): GDistanceMatrixResponse {
                        return Gson().fromJson(content, GDistanceMatrixResponse::class.java)
                    }

                }, object : TransformDataClosure<GDistanceMatrixResponse, GDistanceMatrixResponse> {
                    override fun transformFrom(response: GDistanceMatrixResponse): GDistanceMatrixResponse {
                        return response
                    }
                }))
                .build()
        api.getMatrix(request)
    }

    override fun callDirection(origin: LocationVM, wayPoints: Array<LocationVM>?, dest: LocationVM, callback: AppCallback<DirectionVM>) {
        val originText = origin.lat.toString() + "," + origin.lng.toString()
        val destText = dest.lat.toString() + "," + dest.lng.toString()

        val queryParams = java.util.HashMap<String, String>()
        queryParams["origin"] = originText
        queryParams["destination"] = destText
        queryParams["waypoints"] = convert(wayPoints)
        queryParams["sensor"] = "true"
        queryParams["mode"] = "driving"
        queryParams["key"] = MyApplication.get().getString(R.string.google_map_api_key)

        val request = AppRequest.Builder()
                .get()
                .setPathSegments("json")
                .setQueryParams(queryParams)
                .setResponseListener(ResponseListener(callback, object : ParseClosure<GGDirectionResponse> {
                    override fun parse(call: Call, content: String, okResponse: Response): GGDirectionResponse {
                        return Gson().fromJson(content, GGDirectionResponse::class.java)
                    }

                }, object : OnResponseClosure<GGDirectionResponse> {
                    override fun onResponse(response: GGDirectionResponse) {
                        val status = response.status!!.toUpperCase()

                        when (status) {
                            "OK" -> {
                                val vm = transform(response)
                                onMain(Runnable {
                                    callback.onSuccess(vm)
                                })
                            }
                            else -> onMain(Runnable {
                                callback.onFailed(response.errorMessage!!)
                            })
                        }
                    }
                }))
                .build()

        api.getGGDirection(request)
    }

    private fun convert(locations: Array<LocationVM>?): String {
        locations ?: return ""

        var result = ""

        locations.forEachIndexed { index, locationVM ->
            result += locationVM.lat.toString() + "," + locationVM.lng.toString()

            if (index < locations.size - 1)
                result += "|"
        }

        return result
    }

    private fun transform(response: GGDirectionResponse): DirectionVM {
        val directionVM = DirectionVM()
        val routes = response.routes!!
        val allSteps = java.util.ArrayList<DirectionVM.Step>()

        var totalDurations = BigInteger.ZERO
        var totalDistance = BigInteger.ZERO

        val transformer = Utils.TransformerV2(object : Utils.NewObjectClosure<LocationVM> {
            override fun makeNew(): LocationVM {
                return LocationVM()
            }

        }, object : Utils.InitObjectClosure<LocationVM, LatLng> {
            override fun initDest(dest: LocationVM, source: LatLng) {
                dest.lat = source.latitude.toFloat()
                dest.lng = source.longitude.toFloat()
            }

        }, null)

        val route = routes[0]

        for (leg in route.legs!!) {
            totalDurations = totalDurations.add(BigInteger.valueOf(leg.duration!!.value.toLong()))
            totalDistance = totalDistance.add(BigInteger.valueOf(leg.distance!!.value.toLong()))

            val steps = leg.steps!!

            for (step in steps) {
                val latLngs = PolyUtil.decode(step.polyline!!.points)
                val locationInfos = Utils.transform(latLngs, transformer)

                val tempStep = DirectionVM.Step()
                tempStep.locations = locationInfos
                allSteps.add(tempStep)
            }
        }

        directionVM.distance = totalDistance
        directionVM.duration = totalDurations
        directionVM.errorMessage = response.errorMessage
        directionVM.steps = allSteps

        return directionVM
    }

}