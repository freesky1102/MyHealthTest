package com.project.api

import com.project.api.errors.ServerError
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException

class CallbackWrapper<R>(private var listener: ResponseListener<*, R, *>?) : Callback {

    @Throws(IOException::class)
    override fun onResponse(call: Call, response: Response) {

        if (this.listener != null) {

            val statusCode = response.code()

            var isError = false

            if (statusCode < 200 || statusCode > 299) {
                isError = true
            }
            /*
            If call API successfully
             */
            if (!isError) {
                try {
                    val content = this.listener!!.getContent(call, response)!!
                    val temp = this.listener!!.parse(call, content, response)!!
                    this.listener!!.onResponse(temp)
                } catch (e: Exception) {
                    e.printStackTrace()
                    this.listener!!.onError(e)
                }

                response.close()

                this.listener = null

                return
            }

            /*
            If error occurs
             */
            this.listener!!.onError(ServerError(call, response))
            this.listener = null
        }
    }

    override fun onFailure(call: Call, e: IOException) {
        e.printStackTrace()

        if (this.listener != null) {
            this.listener!!.onError(e)
        }

        this.listener = null
    }
}