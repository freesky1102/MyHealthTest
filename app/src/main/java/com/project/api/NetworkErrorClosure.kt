package com.project.api

import android.support.annotation.MainThread

interface NetworkErrorClosure {

    @MainThread
    fun onNetworkError(message: String)

}
