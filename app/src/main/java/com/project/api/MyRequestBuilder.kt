package com.project.api

import okhttp3.*
import java.util.*

internal class MyRequestBuilder private constructor() {

    private lateinit var okRequestBuilder: Request.Builder

    private lateinit var urlBuilder: HttpUrl.Builder

    private var pathSegments: String? = null

    var method: String? = null
        private set

    private var beforeEnqueueChains: MutableList<BeforeEnqueueChain>? = null

    fun getPathSegments(): String? {
        return pathSegments
    }

    fun setPathSegments(pathSegments: String): MyRequestBuilder {
        if (pathSegments.startsWith("/")) {
            throw RuntimeException("Path Segments is not started with / !")
        }

        this.pathSegments = pathSegments
        urlBuilder.addPathSegments(pathSegments)

        return this
    }

    fun get(): MyRequestBuilder {
        this.method = METHOD_GET
        okRequestBuilder.get()

        return this
    }

    fun post(body: String): MyRequestBuilder {
        this.method = METHOD_POST
        val JSON = MediaType.parse("application/json; charset=utf-8")
        val requestBody = RequestBody.create(JSON, body)
        okRequestBuilder.post(requestBody)

        return this
    }

    fun headers(headers: Map<String, String>?): MyRequestBuilder {

        if (headers != null) {
            for ((key, value) in headers) {
                okRequestBuilder.header(key, value)
            }
        }

        return this
    }

    fun queryParams(queryParams: Map<String, String>?): MyRequestBuilder {

        if (queryParams != null) {
            for ((key, value) in queryParams) {
                urlBuilder.addQueryParameter(key, value)
            }
        }

        return this
    }

    fun beforeEnqueue(beforeEnqueueChain: BeforeEnqueueChain): MyRequestBuilder {
        if (beforeEnqueueChains == null) {
            beforeEnqueueChains = ArrayList()
        }

        this.beforeEnqueueChains!!.add(beforeEnqueueChain)
        return this
    }

    fun enqueue(okHttpClient: OkHttpClient, responseListener: ResponseListener<*, *, *>) {
        okRequestBuilder.url(urlBuilder.build())

        if (beforeEnqueueChains != null) {
            for (chain in beforeEnqueueChains!!) {
                try {
                    chain.config(this)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            beforeEnqueueChains!!.clear()
        }

        okHttpClient.newCall(okRequestBuilder.build()).enqueue(
                CallbackWrapper(responseListener))
    }

    interface BeforeEnqueueChain {

        @Throws(Exception::class)
        fun config(requestBuilder: MyRequestBuilder)

    }

    companion object {

        val METHOD_POST = "POST"

        val METHOD_GET = "GET"

        fun newInstance(baseUrl: String): MyRequestBuilder {

            val requestBuilder = MyRequestBuilder()

            requestBuilder.okRequestBuilder = Request.Builder()
            requestBuilder.urlBuilder = HttpUrl.parse(baseUrl)!!.newBuilder()
            return requestBuilder
        }
    }
}