package com.project.api

import android.support.annotation.NonNull
import android.support.annotation.WorkerThread

interface OnErrorClosure {

    @WorkerThread
    fun onError(@NonNull e: Exception)

}