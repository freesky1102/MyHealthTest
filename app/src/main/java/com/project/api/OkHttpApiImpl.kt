package com.project.api

import android.text.TextUtils
import com.project.AppConfig
import okhttp3.OkHttpClient

/**
 * Created by freesky1102 on 7/15/16.
 */
class OkHttpApiImpl(private val mOkHttpClient: OkHttpClient) : Api {

    override fun getGGDirection(request: AppRequest) {
        makeBuilder(AppConfig.GOOGLE_DIRECTION_URL, request)
                .enqueue(mOkHttpClient, request.responseListener)
    }

    override fun getLocations(request: AppRequest) {
        makeBuilder(AppConfig.MY_JSON_URL, request)
                .enqueue(mOkHttpClient, request.responseListener)
    }

    override fun getMatrix(request: AppRequest) {
        makeBuilder(AppConfig.GOOGLE_MATRIX_URL, request)
                .enqueue(mOkHttpClient, request.responseListener)
    }

    private fun makeBuilder(mainUrl: String, request: AppRequest): MyRequestBuilder {

        var pathSegments = request.pathSegments
        if (TextUtils.isEmpty(pathSegments)) pathSegments = ""

        val builder = MyRequestBuilder.newInstance(mainUrl).setPathSegments(pathSegments!!)

        when (request.requestMethod) {
            AppRequest.GET -> builder.get()
            AppRequest.POST -> builder.post(request.body!!)
        }

        builder.headers(request.headers).queryParams(request.queryParams)

        return builder
    }
}
