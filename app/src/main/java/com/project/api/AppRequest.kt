package com.project.api

class AppRequest private constructor(
        val headers: Map<String, String>?,
        val queryParams: Map<String, String>?,
        val pathSegments: String?,
        val requestMethod: Int,
        val responseListener: ResponseListener<*, *, *>,
        val body: String?) {

    class Builder {

        private var mHeaders: Map<String, String>? = null
        private var mQueryParams: Map<String, String>? = null
        private var mPathSegments: String? = null
        private var mRequestMethod: Int = 0
        private var mV2ResponseListener: ResponseListener<*, *, *>? = null
        private var mBody: String? = null

        fun setHeaders(headers: Map<String, String>): Builder {
            mHeaders = headers
            return this
        }

        fun setQueryParams(queryParams: Map<String, String>): Builder {
            mQueryParams = queryParams
            return this
        }

        fun setPathSegments(pathSegments: String): Builder {
            mPathSegments = pathSegments
            return this
        }

        fun get(): Builder {
            mRequestMethod = GET
            return this
        }

        fun post(body: String): Builder {
            mRequestMethod = POST
            mBody = body
            return this
        }

        fun setResponseListener(responseListener: ResponseListener<*, *, *>): Builder {
            mV2ResponseListener = responseListener
            return this
        }

        fun build(): AppRequest {
            return AppRequest(mHeaders, mQueryParams, mPathSegments, mRequestMethod,
                    mV2ResponseListener!!, mBody)
        }
    }

    companion object {

        val GET = 1

        val POST = 2
    }
}
