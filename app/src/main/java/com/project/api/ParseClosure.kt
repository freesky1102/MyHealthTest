package com.project.api

import android.support.annotation.WorkerThread
import okhttp3.Call
import okhttp3.Response

interface ParseClosure<out R> {

    @WorkerThread
    @Throws(Exception::class)
    fun parse(call: Call, content: String, okResponse: Response): R

}