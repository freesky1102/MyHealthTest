package com.project.api

interface TransformDataClosure<in R, out VM> {

    fun transformFrom(response: R): VM

}
