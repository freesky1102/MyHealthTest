package com.project.screens.map

import android.location.Location
import com.google.android.gms.tasks.OnSuccessListener
import com.project.model.DirectionVM
import com.project.model.LocationVM
import com.project.screens.BaseView

interface MapContract {

    interface View : BaseView {

        fun getLastLocation(successListener: OnSuccessListener<Location>)

        fun focusCamera(location: Location)

        fun focusCameraArea(locations: ArrayList<LocationVM>)

        fun setCurrentMarker(location: Location)

        fun showDeliveryMarkers(data: List<LocationVM>)

        fun showDistanceInfo(distanceMessage: String)

        fun drawPath(data: DirectionVM)

    }

    interface Presenter {

        fun getCurrentLocation()

        fun setLocationPermissionRequested(success: Boolean)

        fun setMapReady(ready: Boolean)

        fun onClickDeliveryLocations()

        fun onClickGetDistances()

        fun onClickFindBestRoute()
    }

}