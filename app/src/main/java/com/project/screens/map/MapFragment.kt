package com.project.screens.map

import android.Manifest
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import co.core.androidpermission.PermissionChecker
import com.base.R
import com.base.fragments.BaseAppFragment
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnSuccessListener
import com.project.dialog.AppDialogFragment
import com.project.ggmap.FusedLocationReceiver
import com.project.ggmap.FusedLocationService
import com.project.model.DirectionVM
import com.project.model.LocationVM
import com.project.screens.map.drawer.DrawerDelegate
import com.project.util.ThreadUtils.onMain
import com.project.util.ThreadUtils.onWorker

/**
 * Created by freesky1102 on 3/17/17.
 */
class MapFragment : BaseAppFragment(), MapContract.View {

    private lateinit var presenter: MapPresenter

    private lateinit var drawerDelegate: DrawerDelegate

    private val fusedLocationService: FusedLocationService by lazy {
        FusedLocationService(LocationServices.getFusedLocationProviderClient(activity!!), activity!!)
    }

    private var map: GoogleMap? = null

    private var currentMarker: Marker? = null

    private var deliveryMarkers: ArrayList<Marker>? = null

    private var mapPolyline: ArrayList<Polyline> = ArrayList()

    override val isHasActionbar: Boolean
        get() = false

    override val layoutRes: Int
        get() = R.layout.fragment_map

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = MapPresenter(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(mapReadyCallback)

        drawerDelegate = DrawerDelegate(activity as AppCompatActivity,
                view.findViewById(R.id.drawer_layout),
                view.findViewById(R.id.left_drawer),
                view.findViewById(R.id.content_layout))
        drawerDelegate.init()

        view.findViewById<View>(R.id.get_delivery_locations).setOnClickListener(onClickDeliveryLocations)
        view.findViewById<View>(R.id.get_distances).setOnClickListener(onClickGetDistances)
        view.findViewById<View>(R.id.find_best_route).setOnClickListener(onClickFindBestRoute)
    }

    private val onClickDeliveryLocations = View.OnClickListener {
        drawerDelegate.closeDrawer(DrawerDelegate.LEFT_DRAWER)
        presenter.onClickDeliveryLocations()
    }

    private val onClickGetDistances = View.OnClickListener {
        drawerDelegate.closeDrawer(DrawerDelegate.LEFT_DRAWER)
        presenter.onClickGetDistances()
    }

    private val onClickFindBestRoute = View.OnClickListener {
        drawerDelegate.closeDrawer(DrawerDelegate.LEFT_DRAWER)
        presenter.onClickFindBestRoute()
    }

    private val mapReadyCallback: OnMapReadyCallback = OnMapReadyCallback {
        map = it
        presenter.setMapReady(true)
        presenter.getCurrentLocation()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val pers = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
        PermissionChecker.checkPermission(pers, this, RC_CHECK_LOCATION_PER)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == RC_CHECK_LOCATION_PER) {
            PermissionChecker.onRequestPermissionResult(permissions, grantResults, object : PermissionChecker.OnCheckPersResultListener {

                override fun onGranted(pers: ArrayList<String>) {
                    presenter.setLocationPermissionRequested(true)
                    presenter.getCurrentLocation()
                }

                override fun onDenied(pers: ArrayList<String>) {
                    Toast.makeText(activity, "you have to grant permission", Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    override fun getLastLocation(successListener: OnSuccessListener<Location>) {
        fusedLocationService.getLastLocation(FusedLocationReceiver.Builder()
                .setOnSuccessListener(successListener).build())
    }

    override fun displayNetworkError(errorMessage: String) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun displayUnexpectedError(errorMessage: String) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun displayTimeoutError(errorMessage: String) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun focusCamera(location: Location) {
        map?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 16.0f))
    }

    override fun setCurrentMarker(location: Location) {
        val latLng = LatLng(location.latitude, location.longitude)

        currentMarker?.remove()

        val iconCurrentMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)

        currentMarker = map?.addMarker(MarkerOptions().position(latLng).icon(iconCurrentMarker))
    }

    override fun showDeliveryMarkers(data: List<LocationVM>) {
        deliveryMarkers?.let {
            for (marker in deliveryMarkers!!) {
                marker.remove()
            }
        }

        deliveryMarkers = ArrayList()

        val iconCurrentMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)

        for (location in data) {
            val latLng = LatLng(location.lat.toDouble(), location.lng.toDouble())
            val marker = map!!.addMarker(MarkerOptions().position(latLng).icon(iconCurrentMarker))
            deliveryMarkers?.add(marker)
        }
    }

    override fun focusCameraArea(locations: ArrayList<LocationVM>) {
        val builder = LatLngBounds.Builder()

        for (location in locations) {
            builder.include(LatLng(location.lat.toDouble(), location.lng.toDouble()))
        }

        map?.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100))
    }

    override fun showDistanceInfo(distanceMessage: String) {

        val dialog = AppDialogFragment.Builder()
                .setCancelable(true)
                .setMessage(distanceMessage)
                .setPositiveText("OK")
                .setTitle("Distances")
                .build()

        showDialog(dialog)
    }

    override fun drawPath(data: DirectionVM) {
        for (polyline in mapPolyline) {
            polyline.remove()
        }

        mapPolyline.clear()
        onWorker(Runnable {
            for (step in data.steps!!) {

                val polyLineOptions = PolylineOptions().color(
                        resources.getColor(android.R.color.holo_green_dark))

                for (locationInfo in step.locations!!) {
                    polyLineOptions.add(convert(locationInfo))
                }

                onMain(Runnable {
                    mapPolyline.add(map!!.addPolyline(polyLineOptions))
                })
            }
        })
    }

    private fun convert(locationInfo: LocationVM): LatLng {
        return LatLng(locationInfo.lat.toDouble(), locationInfo.lng.toDouble())
    }

    companion object {
        const val RC_CHECK_LOCATION_PER = 101
    }
}
