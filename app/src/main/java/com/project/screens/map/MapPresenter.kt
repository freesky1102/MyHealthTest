package com.project.screens.map

import android.location.Location
import com.google.android.gms.tasks.OnSuccessListener
import com.project.AppRepos
import com.project.api.SuccessClosure
import com.project.api.source.AppCallback
import com.project.api.source.DirectionDataSource
import com.project.model.DirectionVM
import com.project.model.LocationVM
import com.project.model.param.MatrixParam
import com.project.util.ThreadUtils.onWorker
import com.tsp.de.Path
import com.tsp.de.TspSolver
import com.tsp.eu.GDistanceMatrixResponse

class MapPresenter : MapContract.Presenter {

    private val view: MapContract.View

    private val directionDS: DirectionDataSource by lazy { AppRepos.provideDirectionDataSource() }

    private var currentLocation: Location? = null

    private var locationPermissionPermitted: Boolean = false

    private var mapReady: Boolean = false

    private var deliveryLocations: ArrayList<LocationVM>? = null

    private var matrixResponse: GDistanceMatrixResponse? = null

    constructor(view: MapContract.View) {
        this.view = view
    }

    override fun getCurrentLocation() {
        if (!isMapReadyAndAllowedPer()) return

        view.getLastLocation(OnSuccessListener { location ->
            currentLocation = location
            view.focusCamera(location)
            view.setCurrentMarker(location)
        })
    }

    private fun callLocations() {
        view.showLoadingBar()

        directionDS.callLocations(AppCallback(view, object : SuccessClosure<ArrayList<LocationVM>> {
            override fun onSuccess(data: ArrayList<LocationVM>) {
                deliveryLocations = data
                view.hideLoadingBar()
                view.showDeliveryMarkers(data)

                val locations = ArrayList<LocationVM>()
                locations.addAll(data)
                currentLocation?.let {
                    locations.add(LocationVM(it.latitude.toFloat(), it.longitude.toFloat()))
                }

                view.focusCameraArea(locations)
            }
        }))
    }

    private fun callMatrix(data: List<LocationVM>) {
        val origins = getAllPoints()
        val matrixParam = MatrixParam(origins, origins)

        view.showLoadingBar()

        directionDS.callMatrix(matrixParam, AppCallback(view, object : SuccessClosure<GDistanceMatrixResponse> {
            override fun onSuccess(data: GDistanceMatrixResponse) {
                matrixResponse = data
                view.hideLoadingBar()
                view.showDistanceInfo(getDistanceMessage(data))
            }
        }))
    }

    private fun getAllPoints(): Array<LocationVM>? {
        val location = currentLocation
        location ?: return null

        val deliveryLocations = deliveryLocations
        deliveryLocations ?: return null

        val origins = ArrayList<LocationVM>()
        origins.add(LocationVM(location.latitude.toFloat(), location.longitude.toFloat()))
        origins.addAll(deliveryLocations)

        return Array(origins.size) { origins[it] }
    }

    private fun getDistanceMessage(data: GDistanceMatrixResponse): String {
        var result = ""

        val originDistanes = data.rows[0]?.elements
        val destinations = data.destinationAddresses

        originDistanes ?: return ""

        for (i in 1 until originDistanes.size) {
            val element = originDistanes[i]
            result += "To " + destinations[i] + " : " + element.distance?.text + "\n"
        }

        return result
    }

    private fun getDistances(response: GDistanceMatrixResponse): Array<DoubleArray> {
        val size = response.rows.size
        val distances = Array(size) { DoubleArray(size) }
        for (i in 0 until size) {
            for (j in 0 until size) {
                distances[i][j] = response.rows[i].elements[j]?.distance?.value ?: 0.0
            }
        }
        return distances
    }

    private fun buildResult(p: Path, locations: List<Int>): List<Int> {
        val result = ArrayList<Int>()

        var i = 0
        do {
            result.add(locations[i])
            i = p.To[i]
        } while (i != 0)

        return result
    }

    override fun setLocationPermissionRequested(success: Boolean) {
        this.locationPermissionPermitted = success
    }

    override fun setMapReady(ready: Boolean) {
        this.mapReady = ready
    }

    private fun isMapReadyAndAllowedPer(): Boolean {
        return mapReady && locationPermissionPermitted
    }

    override fun onClickDeliveryLocations() {
        callLocations()
    }

    override fun onClickGetDistances() {
        val deliveryLocations = deliveryLocations
        if (deliveryLocations == null) {
            view.showDistanceInfo("Please get delivery locations")
            return
        }

        callMatrix(deliveryLocations)
    }

    override fun onClickFindBestRoute() {
        val location = currentLocation
        val deliveryLocations = deliveryLocations;

        if (location == null) {
            view.showDistanceInfo("Can't get current location")
            return
        }

        if (deliveryLocations == null) {
            view.showDistanceInfo("Please get delivery locations")
            return
        }

        if (matrixResponse == null) {
            view.showDistanceInfo("Please get distances")
            return
        }

        val allPoints = getAllPoints()
        allPoints ?: return

        val indexes = ArrayList<Int>()
        for (i in 0 until allPoints.size) {
            indexes.add(i)
        }

        view.showLoadingBar()

        onWorker(Runnable {
            val distances = getDistances(matrixResponse!!)
            val solver = TspSolver(distances)
            val p = solver.solve(20)
            val result = buildResult(p, indexes)

            val origin = allPoints[result[0]]
            val destination = allPoints[result[result.size - 1]]
            val wayPoints = Array(allPoints.size - 2) {
                allPoints[result[it + 1]]
            }

            directionDS.callDirection(origin, wayPoints, destination, AppCallback(view, object : SuccessClosure<DirectionVM> {
                override fun onSuccess(data: DirectionVM) {
                    view.hideLoadingBar()
                    view.drawPath(data)
                }
            }))
        })
    }
}