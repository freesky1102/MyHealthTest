package com.project.screens.map.drawer

import android.support.v4.app.ActionBarDrawerToggle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.base.R


class DrawerDelegate {

    private var activity: AppCompatActivity

    private var mDrawerLayout: DrawerLayout

    private lateinit var mActionBarDrawerToggle: ActionBarDrawerToggle

    private var mLeftDrawer: View? = null

    private var mRightDrawer: View? = null

    private var mContentView: View

    constructor(activity: AppCompatActivity, mDrawerLayout: DrawerLayout, mLeftDrawer: View?, mContentView: View) {
        this.activity = activity
        this.mDrawerLayout = mDrawerLayout
        this.mLeftDrawer = mLeftDrawer
        this.mContentView = mContentView
    }

    fun init() {
        mActionBarDrawerToggle = object : ActionBarDrawerToggle(activity, mDrawerLayout, R.drawable.abc_cab_background_internal_bg,
                R.string.drawer_open, R.string.drawer_close) {

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                activity.supportInvalidateOptionsMenu()
                mActionBarDrawerToggle.syncState()
            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                activity.supportInvalidateOptionsMenu()
                mActionBarDrawerToggle.syncState()
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                if (drawerView === mRightDrawer)
                    mContentView.translationX = slideOffset * drawerView.width.toFloat() * -1.0f
                else if (drawerView === mLeftDrawer)
                    mContentView.translationX = slideOffset * drawerView.width
                mDrawerLayout.bringChildToFront(drawerView)
                mDrawerLayout.requestLayout()
            }
        }

        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle)
    }

    fun unlockDrawer(drawerMode: Int) {
        var gravity = GravityCompat.START
        if (drawerMode == LEFT_DRAWER) {
            gravity = GravityCompat.START
        } else if (drawerMode == RIGHT_DRAWER) {
            gravity = GravityCompat.END
        }

        if (mDrawerLayout.getDrawerLockMode(gravity).equals(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, gravity)
        }
    }

    fun openDrawer(drawerMode: Int) {
        if (drawerMode == LEFT_DRAWER)
            mDrawerLayout.openDrawer(GravityCompat.START)
        else if (drawerMode == RIGHT_DRAWER)
            mDrawerLayout.openDrawer(GravityCompat.END)
    }

    fun closeDrawer(drawerMode: Int) {
        if (drawerMode == LEFT_DRAWER)
            mDrawerLayout.closeDrawer(GravityCompat.START)
        else if (drawerMode == RIGHT_DRAWER)
            mDrawerLayout.closeDrawer(GravityCompat.END)
    }

    companion object {

        const val LEFT_DRAWER = 1

        const val RIGHT_DRAWER = 2

    }

}