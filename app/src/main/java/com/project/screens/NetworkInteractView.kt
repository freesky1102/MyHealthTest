package com.project.screens

interface NetworkInteractView {

    fun displayNetworkError(errorMessage: String)

    fun displayUnexpectedError(errorMessage: String)

    fun displayTimeoutError(errorMessage: String)

}