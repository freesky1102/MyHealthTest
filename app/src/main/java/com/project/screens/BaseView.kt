package com.project.screens

interface BaseView : NetworkInteractView {

    val isReadyToChangeView: Boolean

    fun showLoadingBar()

    fun hideLoadingBar()

}