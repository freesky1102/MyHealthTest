package com.project.ggmap

import android.location.Location

import com.google.android.gms.tasks.OnCanceledListener
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener

/**
 * Created by freesky1102 on 10/20/15.
 */
class FusedLocationReceiver private constructor(
        internal var onSuccessListener: OnSuccessListener<Location>?,
        internal var failListener: OnFailureListener?,
        internal var cancelListener: OnCanceledListener?,
        internal var onCompleteListener: OnCompleteListener<Location>?) {

    class Builder {

        private var mOnSuccessListener: OnSuccessListener<Location>? = null
        private var mFailListener: OnFailureListener? = null
        private var mCancelListener: OnCanceledListener? = null
        private var mOnCompleteListener: OnCompleteListener<Location>? = null

        fun setOnSuccessListener(onSuccessListener: OnSuccessListener<Location>): Builder {
            mOnSuccessListener = onSuccessListener
            return this
        }

        fun setFailListener(failListener: OnFailureListener): Builder {
            mFailListener = failListener
            return this
        }

        fun setCancelListener(cancelListener: OnCanceledListener): Builder {
            mCancelListener = cancelListener
            return this
        }

        fun setOnCompleteListener(onCompleteListener: OnCompleteListener<Location>): Builder {
            mOnCompleteListener = onCompleteListener
            return this
        }

        fun build(): FusedLocationReceiver {
            return FusedLocationReceiver(mOnSuccessListener, mFailListener, mCancelListener,
                    mOnCompleteListener)
        }
    }
}
