package com.project.ggmap

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

/**
 * Created by freesky1102 on 11/11/15.
 */
class GPSStatusReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val sendToActivity = Intent(GPS_CHANGED)
        context.sendBroadcast(sendToActivity)
    }

    companion object {

        private val GPS_CHANGED = "gps_changed"

        val gpsIntentFilter: IntentFilter
            get() = IntentFilter(GPS_CHANGED)
    }
}
