package com.project.ggmap

import android.Manifest.permission
import android.annotation.SuppressLint
import android.app.Activity
import android.content.IntentSender
import android.location.Location
import android.os.Looper
import android.widget.Toast
import co.core.androidpermission.PermissionChecker
import com.base.R
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task

class FusedLocationService(private val fusedLocationClient: FusedLocationProviderClient,
                           private val activity: Activity) {

    private var lastReceiver: FusedLocationReceiver? = null

    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(locationResult: LocationResult) {
            val currentLocation = locationResult.lastLocation
            executeSuccessReceiver(currentLocation)
        }
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation(receiver: FusedLocationReceiver?) {
        if (receiver == null) return

        if (!PermissionChecker.isGranted(permission.ACCESS_COARSE_LOCATION, activity) && !PermissionChecker.isGranted(permission.ACCESS_FINE_LOCATION, activity)) {

            return
        }

        this.lastReceiver = receiver

        fusedLocationClient.lastLocation.addOnSuccessListener(activity) { location ->
            if (location == null) {
                updateLocation()
                return@addOnSuccessListener
            }

            executeSuccessReceiver(location)
        }.addOnCanceledListener(activity) { this.executeCancelReceiver() }
                .addOnCompleteListener(activity) { this.executeCompleteReceiver(it) }
                .addOnFailureListener(activity) { this.executeFailReceiver(it) }
    }

    private fun executeSuccessReceiver(location: Location?) {
        lastReceiver?.onSuccessListener?.onSuccess(location)
    }

    private fun executeFailReceiver(e: Exception) {
        lastReceiver?.failListener?.onFailure(e)
    }

    private fun executeCompleteReceiver(task: Task<Location>) {
        lastReceiver?.onCompleteListener?.onComplete(task)
    }

    private fun executeCancelReceiver() {
        lastReceiver?.cancelListener?.onCanceled()
    }

    @SuppressLint("MissingPermission")
    private fun updateLocation() {
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(createLocationRequest())

        val settingsClient = LocationServices.getSettingsClient(activity)
        val task = settingsClient.checkLocationSettings(builder.build())
        task.addOnCompleteListener(activity) { task1 ->

            fusedLocationClient.requestLocationUpdates(createLocationRequest(),
                    locationCallback, Looper.getMainLooper())

        }.addOnFailureListener(activity) { e ->
            val statusCode = (e as ApiException).statusCode
            when (statusCode) {
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                    val rae = e as ResolvableApiException
                    rae.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS)
                } catch (sie: IntentSender.SendIntentException) {
                    sie.printStackTrace()
                }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Toast.makeText(activity, R.string.message_fix_location_settings,
                        Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun createLocationRequest(): LocationRequest {
        val mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        return mLocationRequest
    }

    companion object {
        private const val REQUEST_CHECK_SETTINGS = 1000
    }
}