package com.project

import com.project.api.Api
import com.project.api.source.DirectionCloudDataSource
import com.project.api.source.DirectionDataSource

class AppRepos(val api: Api) {

    companion object {
        private lateinit var sInstance: AppRepos

        internal fun setUp(api: Api) {
            sInstance = AppRepos(api)
        }

        fun provideDirectionDataSource(): DirectionDataSource {
            return DirectionCloudDataSource(sInstance.api)
        }
    }
}