package com.project.model

import java.math.BigInteger

class DirectionVM {

    var errorMessage: String? = null

    /**
     * In seconds
     */
    var duration: BigInteger? = null

    /**
     * In meters
     */
    var distance: BigInteger? = null

    var steps: List<Step>? = null

    class Step {

        var locations: List<LocationVM>? = null
    }
}
