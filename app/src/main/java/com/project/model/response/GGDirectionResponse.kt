package com.project.model.response

import com.google.gson.annotations.SerializedName

class GGDirectionResponse {

    @SerializedName("status")
    val status: String? = null

    @SerializedName("routes")
    val routes: List<Route>? = null

    @SerializedName("error_message")
    val errorMessage: String? = null

    class Route {

        @SerializedName("waypoint_order")
        val waypointOrder: List<String>? = null

        @SerializedName("warnings")
        val warnings: List<String>? = null

        @SerializedName("summary")
        val summary: String? = null

        @SerializedName("overview_polyline")
        val overviewPolyline: OverviewPolyline? = null

        @SerializedName("legs")
        val legs: List<Leg>? = null

        @SerializedName("copyrights")
        val copyrights: String? = null

        @SerializedName("bounds")
        val bounds: Bounds? = null
    }

    class OverviewPolyline {

        @SerializedName("points")
        val points: String? = null
    }

    class Leg {
        @SerializedName("via_waypoint")
        val viaWaypoint: List<String>? = null
        @SerializedName("traffic_speed_entry")
        val trafficSpeedEntry: List<String>? = null
        @SerializedName("steps")
        val steps: List<Step>? = null
        @SerializedName("start_location")
        val startLocation: Location? = null
        @SerializedName("start_address")
        val startAddress: String? = null
        @SerializedName("end_location")
        val endLocation: Location? = null
        @SerializedName("end_address")
        val endAddress: String? = null
        @SerializedName("duration")
        val duration: Duration? = null
        @SerializedName("distance")
        val distance: Distance? = null
    }

    class Step {
        @SerializedName("travel_mode")
        val travelMode: String? = null
        @SerializedName("start_location")
        val startLocation: Location? = null
        @SerializedName("polyline")
        val polyline: Polyline? = null
        @SerializedName("maneuver")
        val maneuver: String? = null
        @SerializedName("html_instructions")
        val htmlInstructions: String? = null
        @SerializedName("end_location")
        val endLocation: Location? = null
        @SerializedName("duration")
        val duration: Duration? = null
        @SerializedName("distance")
        val distance: Distance? = null
    }

    class Polyline {
        @SerializedName("points")
        val points: String? = null
    }

    class Location {
        @SerializedName("lng")
        val lng: Double = 0.toDouble()
        @SerializedName("lat")
        val lat: Double = 0.toDouble()
    }

    class Duration {
        @SerializedName("value")
        val value: Int = 0
        @SerializedName("text")
        val text: String? = null
    }

    class Distance {
        @SerializedName("value")
        val value: Int = 0
        @SerializedName("text")
        val text: String? = null
    }

    class Bounds {
        @SerializedName("southwest")
        val southwest: Location? = null
        @SerializedName("northeast")
        val northeast: Location? = null
    }
}
