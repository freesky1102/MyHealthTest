package com.project.model.response

import com.google.gson.annotations.SerializedName
import com.project.model.LocationVM

data class MyJsonResponse(
        @SerializedName("result") val location: ArrayList<LocationVM>? = null
)