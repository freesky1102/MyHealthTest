package com.project.model

import com.google.gson.annotations.SerializedName

class LocationVM {

    @SerializedName("lat")
    var lat: Float

    @SerializedName("lng")
    var lng: Float

    constructor() {
        lat = 0.0F
        lng = 0.0F
    }

    constructor(lat: Float, lng: Float) {
        this.lat = lat
        this.lng = lng
    }
}