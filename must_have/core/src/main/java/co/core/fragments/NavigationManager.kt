package co.core.fragments

import android.support.v4.app.Fragment

/**
 * Created by freesky1102 on 5/27/16.
 */
interface NavigationManager {
    /**
     * Pop fragment in stack if stack isn't empty.

     * @return true if success, false otherwise. (maybe: stack is empty,
     * * activity is in onSaveInstance())
     */
    fun goBack(): Boolean

    val activePage: Fragment?

    fun finishActivity()

    fun showPage(fragment: Fragment)

    fun showPage(fragment: Fragment, hasAnimation: Boolean, isAddBackStack: Boolean)
}
