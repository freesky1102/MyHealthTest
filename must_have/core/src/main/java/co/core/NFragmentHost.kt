package co.core

import co.core.fragments.NavigationManager
import co.core.imageloader.NImageLoader

/**
 * @author TUNGDX
 */
interface NFragmentHost {
    val imageLoader: NImageLoader?

    val navigationManager: NavigationManager?
}
