package co.core.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.DialogFragment

import co.core.toolbox.NotifyUtil

/**
 * Created by freesky1102 on 8/28/16.
 */
abstract class NDialogFragment : DialogFragment() {

    protected var mRequestCode: Int = 0

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle = savedInstanceState ?: arguments

        if (bundle != null)
            getDataFrom(bundle)
    }

    @CallSuper
    open protected fun getDataFrom(bundle: Bundle) {
        mRequestCode = bundle.getInt(EXTRA_REQUEST_CODE)
    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(EXTRA_REQUEST_CODE, mRequestCode)
    }

    @CallSuper
    override fun onCancel(dialog: DialogInterface?) {
        NotifyUtil.notifyAction(false, this, null, mRequestCode, ACTION_CANCEL)
    }

    companion object {

        const val ACTION_CANCEL = -1

        private const val EXTRA_REQUEST_CODE = "extra_request_code"

        fun makeBundle(requestCode: Int): Bundle {
            val bundle = Bundle()
            bundle.putInt(EXTRA_REQUEST_CODE, requestCode)
            return bundle
        }
    }
}
