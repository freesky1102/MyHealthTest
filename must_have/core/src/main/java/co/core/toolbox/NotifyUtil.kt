package co.core.toolbox

import android.content.Intent
import android.support.v4.app.Fragment
import co.core.dialog.NDialogFragment
import co.core.dialog.OnActionInDialogListener
import co.core.fragments.OnFragmentResultListener

/**
 * Created by freesky1102 on 8/28/16.
 * <pre>
 * This class is used to notify events from

 * DialogFragment -> Fragment host
 * Fragment -> target Fragment
</pre> *
 */
object NotifyUtil {

    fun notifyAction(isDismiss: Boolean, dialog: NDialogFragment?, intent: Intent?, requestCode: Int, action: Int) {
        if (dialog == null) {
            throw IllegalStateException("Dialog cannot be null !")
        }

        if (isDismiss) dialog.dismiss()

        val responseFragment = dialog.parentFragment
        if (responseFragment != null && responseFragment is OnActionInDialogListener) {
            responseFragment.onDialogResult(requestCode, action, intent)
            return
        }

        val activity = dialog.activity
        if (activity != null && activity is OnActionInDialogListener) {
            activity.onDialogResult(requestCode, action, intent)
        }
    }

    fun notifyFragmentAction(fragment: Fragment?, action: Int, intent: Intent?) {
        if (fragment == null) return

        val sourceFragment = fragment.targetFragment
        if (sourceFragment == null || sourceFragment !is OnFragmentResultListener) return

        sourceFragment.onFragmentResult(fragment.targetRequestCode, action, intent)
    }
}
