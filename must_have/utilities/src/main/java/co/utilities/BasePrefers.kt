package co.utilities

import android.content.Context
import android.content.SharedPreferences

abstract class BasePrefers protected constructor(context: Context) {
    protected var mEditor: SharedPreferences.Editor
    var sharedPreference: SharedPreferences
        protected set
    protected var mContext: Context

    init {
        mContext = context.applicationContext
        sharedPreference = mContext.getSharedPreferences(fileNamePrefers,
                Context.MODE_PRIVATE)
        mEditor = sharedPreference.edit()
    }

    protected abstract val fileNamePrefers: String
}
